package homework1

import com.google.gson.Gson
import com.itextpdf.text.Document
import com.itextpdf.text.Rectangle
import com.itextpdf.text.pdf.PdfFormField
import com.itextpdf.text.pdf.PdfWriter
import homework1.Logger.Companion.loggerError
import homework1.Logger.Companion.loggerFileCreate
import java.io.File
import java.io.FileOutputStream
import kotlin.system.exitProcess


fun validateInput(inputData: String?): Int {
    val number = inputData?.toIntOrNull()  // преобразуем введенную строку в число
    try {
        when {
            number == null -> throw IllegalArgumentException("Ошибка: нужно вводить цифры")
            (number < 0 || number > 50) -> {
                throw IllegalArgumentException("Ошибка: нужно ввести цифры в диапазоне от 0 до 50")
            }
        }
    } catch (e: IllegalArgumentException) {
        loggerError.error(e.toString())
        exitProcess(1)
    }
    return number!!
}

fun main() {

    print("Введите число от 0 до 50: ")
    val inputData = readLine() // считываем строк

    val peoplesNumber = validateInput(inputData)
    val listOfPeoples = PersonDataCreator.listOfPeoplesCreator(peoplesNumber)

    // Создание JSON
    val gson = Gson()
    val fileJson = File("data.json")
    val jsonString = gson.toJson(listOfPeoples)
    fileJson.writeText(jsonString)
    loggerFileCreate.info("Файл JSON создан. Путь ${fileJson.absolutePath} ")

    // Создание документа PDF
    val document = Document()
    val filePdf = File("table.pdf")


    PdfWriter.getInstance(document, FileOutputStream(filePdf))
    document.open()

    val tableCreator = TableCreator()
    val table = tableCreator.getTable()

    tableCreator.addTitleToTable()
    tableCreator.addPeoplesToTable(listOfPeoples)
    document.add(table)
    document.close()
    loggerFileCreate.info("Путь: ${filePdf.absolutePath}")
}
