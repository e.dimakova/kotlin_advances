package homework1

import com.itextpdf.text.Font
import com.itextpdf.text.Phrase
import com.itextpdf.text.pdf.BaseFont
import com.itextpdf.text.pdf.PdfPCell
import com.itextpdf.text.pdf.PdfPTable

class TableCreator {
    private val table = PdfPTable(14)
    private val font = BaseFont.createFont("FreeSans.ttf", BaseFont.IDENTITY_H, BaseFont.EMBEDDED)
    private fun addCell(text: String) {
        table.addCell(PdfPCell(Phrase(text, Font(font, 6f))))
    }

    fun getTable(): PdfPTable {
        return table
    }

    fun addTitleToTable() {
        addCell("Имя")
        addCell("Фамилия")
        addCell("Отчество")
        addCell("Пол")
        addCell("Дата рождения")
        addCell("Возраст")
        addCell("Место рождения")
        addCell("Индекс")
        addCell("Страна")
        addCell("Область")
        addCell("Город")
        addCell("Улица")
        addCell("Дом")
        addCell("Квартира")
    }

    fun addPeoplesToTable(peoples: MutableList<Person>) {
        var item = peoples.size - 1
        while (item >= 0) {
            addCell(peoples[item].firstName)
            addCell(peoples[item].lastName)
            addCell(peoples[item].middleName)
            addCell(peoples[item].gender)
            addCell(peoples[item].dataOfBirth)
            addCell(peoples[item].age.toString())
            addCell(peoples[item].placeOfBirth)
            addCell(peoples[item].address.zipCode)
            addCell(peoples[item].address.country)
            addCell(peoples[item].address.region)
            addCell(peoples[item].address.city)
            addCell(peoples[item].address.street)
            addCell(peoples[item].address.buildingNumber)
            addCell(peoples[item].address.apartmentNumber)
            item -= 1
        }
    }

}