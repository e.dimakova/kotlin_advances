package kotlin_advanced_1_11

class Example {
    companion object {
        private var count = 0
        fun counter() {
            count += 1
            println("Вызван counter. Количество вызовов = $count")
        }
    }
}
