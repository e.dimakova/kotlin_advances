package homework1

import org.slf4j.Logger
import org.slf4j.LoggerFactory

class Logger {
    companion object {
        val loggerFileCreate: Logger =  LoggerFactory.getLogger("Файл создан")
        val loggerError: Logger =  LoggerFactory.getLogger("Программа экстренно завершена")
    }
}