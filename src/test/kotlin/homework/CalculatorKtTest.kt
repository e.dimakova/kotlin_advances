package homework

import junit.framework.TestCase.assertEquals
import kotlin_advanced_2_7.*
import org.junit.Test
import org.junit.runner.RunWith
import org.junit.runners.Parameterized

@RunWith(Parameterized::class)
class CalculatorKtTest(
    private val firstOperand: Double,
    private val secondOperand: Double,
    private val sumExpectedResult: Double,
    private val minusExpectedResult: Double,
    private val multiplicationExpectedResult: Double,
    private val divisionExpectedResult: Double
) {
    companion object {
        @JvmStatic
        @Parameterized.Parameters()
        fun data(): List<Array<Double>> {
            return listOf(
                arrayOf(1.999, 2.001, 4.0, -0.002, 3.999999, 0.9990004997501251),
                arrayOf(-2.2, 7.9, 5.7, -10.1, -17.38, -0.27848101265822783),
                arrayOf(6.0, 0.0, 6.0, 6.0, 0.0, Double.POSITIVE_INFINITY),
                arrayOf(Double.MAX_VALUE, Double.MAX_VALUE, Double.POSITIVE_INFINITY, 0.0, Double.POSITIVE_INFINITY, 1.0),
                arrayOf(Double.MIN_VALUE, Double.MIN_VALUE, 1.0E-323, 0.0, 0.0, 1.0)
            )
        }
    }

    @Test
    fun checkSum() {
        assertEquals(
            "Error in expression: ($firstOperand + $secondOperand)",
            sumExpectedResult,
            calculator(sum, firstOperand, secondOperand)
        )
    }

    @Test
    fun checkSubtraction() {
        assertEquals(
            "Error in expression: ($firstOperand - $secondOperand)",
            minusExpectedResult,
            calculator(subtraction, firstOperand, secondOperand)
        )
    }

    @Test
    fun checkMultiplication() {
        assertEquals(
            "Error in expression: ($firstOperand * $secondOperand)",
            multiplicationExpectedResult,
            calculator(multiplication, firstOperand, secondOperand)
        )
    }

    @Test
    fun checkDivision() {
        assertEquals(
            "Error in expression: ($firstOperand / $secondOperand)",
            divisionExpectedResult,
            calculator(division, firstOperand, secondOperand)
        )
    }
}
