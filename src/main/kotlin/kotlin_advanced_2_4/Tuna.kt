package kotlin_advanced_2_4

class Tuna : Fish() {
    override fun swim() {
        println("I am a Tuna, and I am swimming")
    }
}
