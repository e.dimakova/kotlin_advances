package homework1

import java.time.LocalDate
import java.time.Period
import java.time.format.DateTimeFormatter

object PersonDataCreator {

    private val dateFormatter: DateTimeFormatter = DateTimeFormatter.ofPattern("dd-MM-yyyy")

    fun listOfPeoplesCreator(peopleNumber: Int): MutableList<Person> {
        val peoples: MutableList<Person> = mutableListOf()
        var item = peopleNumber
        while (item > 0) {
            val person = personCreator()
            peoples.add(person)
            item -= 1
        }
        return peoples

    }
    
    private fun LocalDate.toFormat(): String {
        return this.format(dateFormatter)
    }

    private fun bDayCreator(): LocalDate {
        val numberOfYears: Long = 100
        val today = LocalDate.now()                   // сегодня
        val fewYearsAgo = today.minusYears(numberOfYears)  // 100 лет назад
        return today.plusDays((Math.random() * (fewYearsAgo.toEpochDay() - today.toEpochDay())).toLong())
    }

    private fun addressCreator(): Address {
        return Address(
            RandomDataCreator.zipCode(),
            RandomDataCreator.country(),
            RandomDataCreator.region(),
            RandomDataCreator.city(),
            RandomDataCreator.street(),
            RandomDataCreator.buildingNumber(),
            RandomDataCreator.apartmentNumber()
        )
    }

    private fun personCreator(): Person {
        val gender = listOf(Gender.MALE, Gender.FEMALE).random()
        val bDay = bDayCreator()
        val age = Period.between(bDay, LocalDate.now()).years
        return if (gender == Gender.MALE) {
            Person(
                RandomDataCreator.maleFirstName(),
                RandomDataCreator.maleLastName(),
                RandomDataCreator.maleMiddleName(),
                gender.value,
                bDay.toFormat(),
                age,
                RandomDataCreator.placeOfBirth(),
                addressCreator()
            )
        } else {
            Person(
                RandomDataCreator.femaleFirstName(),
                RandomDataCreator.femaleLastName(),
                RandomDataCreator.femaleMiddleName(),
                gender.value,
                bDay.toFormat(),
                age,
                RandomDataCreator.placeOfBirth(),
                addressCreator()
            )
        }
    }
}
