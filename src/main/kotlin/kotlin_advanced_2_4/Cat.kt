package kotlin_advanced_2_4

open class Cat : Pet(), Runnable, Swimmable {
    override fun run() {
        println("I am a Cat, and I am running")
    }

    override fun swim() {
        println("I am a Cat, and I am swimming")
    }
}
