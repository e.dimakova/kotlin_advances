package kotlin_advanced_2_4

class Tiger : Cat() {
    override fun run() {
        println("I am a Tiger, and I am running")
    }

    override fun swim() {
        println("I am a Tiger, and I am swimming")
    }
}
