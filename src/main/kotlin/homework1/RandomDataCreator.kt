package homework1

import com.github.javafaker.Faker
import java.util.*


object RandomDataCreator {

    private val faker = Faker(Locale("ru"))

    fun maleFirstName(): String {
        return faker.resolve("name.male_first_name")
    }

    fun maleMiddleName(): String {
        return faker.resolve("name.male_middle_name")
    }

    fun maleLastName(): String {
        return faker.resolve("name.male_last_name")
    }

    fun femaleMiddleName(): String {
        return faker.resolve("name.female_middle_name")
    }

    fun femaleFirstName(): String {
        return faker.resolve("name.female_first_name")
    }

    fun femaleLastName(): String {
        return faker.resolve("name.female_last_name")
    }

    fun placeOfBirth(): String {
        return faker.resolve("address.city_name")
    }

    fun zipCode(): String {
        return faker.address().zipCode()
    }

    fun country(): String {
        return faker.address().country()
    }

    fun region(): String {
        return faker.address().state()
    }

    fun city(): String {
        return faker.address().city()
    }

    fun street(): String {
        return faker.address().streetName()
    }

    fun buildingNumber(): String {
        return faker.address().buildingNumber()
    }

    fun apartmentNumber(): String {
          return faker.address().secondaryAddress()
    }
}

