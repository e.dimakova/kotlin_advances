package kotlin_advanced_1_5

fun String?.getOrEmpty(data: String): String {
    return if (this != null) {
        ", $data = $this"
    } else ""
}

fun createUser(
    name: String,
    surname: String,
    patronymic: String? = null,
    gender: String,
    age: Int,
    dateOfBirth: String,
    inn: String? = null,
    snils: String? = null
) {
    println(
        "name = $name, surname = $surname${patronymic.getOrEmpty("patronymic")}, gender = $gender, age = $age, dateOfBirth = $dateOfBirth${
            inn.getOrEmpty(
                "inn"
            )
        }${snils.getOrEmpty("snils")}"
    )
}

fun main() {
    println("Пользователь 1")
    createUser(name = "Van", surname = "Darkholml", gender = "Male", age = 43, dateOfBirth = "1980-01-04")
    println("Пользователь 2")
    createUser(
        age = 63,
        name = "Igor",
        inn = "1234567890",
        gender = "FiveReasonMan",
        surname = "Nikolaev",
        patronymic = "Vladimirovich",
        dateOfBirth = "1960-01-30",
        snils = "111-111-111"
    )
}
