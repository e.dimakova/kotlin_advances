package kotlin_advanced_2_4

open class Fish : Pet(), Swimmable {
    override fun swim() {
        println("I am a Cat, and I am swimming")
    }
}
