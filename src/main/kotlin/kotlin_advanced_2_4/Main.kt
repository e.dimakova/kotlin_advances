package kotlin_advanced_2_4

fun useRunSkill(t: Runnable) {
    t.run()
}

fun useSwimSkill(t: Swimmable) {
    t.swim()
}

fun useSwimAndRunSkill(t: Cat) {
    t.run()
    t.swim()
}

fun main() {
    val tiger = Tiger()
    useRunSkill(tiger)
    useSwimSkill(tiger)
    useSwimAndRunSkill(tiger)

    val salmon = Salmon()
    useSwimSkill(salmon)

    val lion = Lion()
    useRunSkill(lion)
    useSwimSkill(lion)
    useSwimAndRunSkill(lion)

    val tuna = Tuna()
    useSwimSkill(tuna)
}
