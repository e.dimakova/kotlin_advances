package kotlin_advanced_1_3
fun Int.square(): Int {
    return this * this
}

fun MutableList<Int>.square(): MutableList<Int> {
    val newList = mutableListOf<Int>()
    for (item in this) {
        newList.add(item.square())
    }
    return newList
}

fun main() {
    val myArray = mutableListOf(1, 4, 9, 16, 25)
    println(myArray.square())
}
