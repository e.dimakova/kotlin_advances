package kotlin_advanced_2_8

fun String.isNumeric() : Boolean {
    return this.toDoubleOrNull() != null
}

fun String.strToListInt(): List<Int> {
    return this.map { it -> it.toString().toInt() }
}

fun findControlNumber(inn: String, controlNumber: Int): Int {
    return inn.strToListInt()[controlNumber - 1]
}

fun findMagicNumber(list1: String, list2: List<Int>): Int {
    val newCollection = list1.strToListInt().zip(list2) { a, b -> a * b }
    return (newCollection.sum() % 11 % 10)
}

val firstCoefficient = listOf(7, 2, 4, 10, 3, 5, 9, 4, 6, 8)
val secondCoefficient = listOf(3, 7, 2, 4, 10, 3, 5, 9, 4, 6, 8)

val validateInn = { inn: String ->
    if (inn.length != 12 || !inn.isNumeric()) false else
        findMagicNumber(inn, firstCoefficient) == findControlNumber(inn, 11) &&
                findMagicNumber(inn, secondCoefficient) == findControlNumber(inn, 12)
}

fun validateInnFunction(inn: String, function: (String) -> Boolean) {
    println("ИНН $inn ${if (function(inn)) "валиден" else "не валиден"}")
}

fun main() {
    val inn = "707574944998"
    validateInnFunction(inn, validateInn)
}
