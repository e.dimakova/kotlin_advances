package homework1

data class Address(
    val zipCode: String,
    val country: String,
    val region: String,
    val city: String,
    val street: String,
    val buildingNumber: String,
    val apartmentNumber: String
    )
