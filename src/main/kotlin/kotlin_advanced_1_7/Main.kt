package kotlin_advanced_1_7

fun elementsInfo(vararg param: String)  {
    var count = 0
    param.forEach { _ -> count += 1 }
    println("Передано $count элемента: \n${param.joinToString(separator = ";", postfix = ";")}")
}

fun main(){
    elementsInfo("12","122","1234", "fpo")
}
