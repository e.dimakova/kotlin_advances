package homework1

enum class Gender(val value: String) {
    FEMALE("женский"),
    MALE("мужской")
}