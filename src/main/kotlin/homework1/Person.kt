package homework1

data class Person(
    val firstName: String,
    val lastName: String,
    val middleName: String,
    val gender: String,
    val dataOfBirth: String,
    var age: Int,
    val placeOfBirth: String,
    var address: Address
)
