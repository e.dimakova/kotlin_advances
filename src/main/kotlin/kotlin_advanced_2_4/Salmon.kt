package kotlin_advanced_2_4

class Salmon : Fish() {
    override fun swim() {
        println("I am a Salmon, and I am swimming")
    }
}
