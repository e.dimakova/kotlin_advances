package kotlin_advanced_2_4

class Lion : Cat() {
    override fun run() {
        println("I am a Lion, and I am running")
    }

    override fun swim() {
        println("I am a Lion, and I am swimming")
    }
}
