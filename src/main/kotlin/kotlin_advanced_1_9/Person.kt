package kotlin_advanced_1_9

data class Person(
    val name: String,
    val surname: String,
    val patronymic: String? = null,
    val gender: String,
    var age: Int,
    val dateOfBirth: String,
    val inn: String? = null,
    val snils: String? = null
)
