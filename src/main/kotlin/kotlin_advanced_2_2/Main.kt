package kotlin_advanced_2_2

import kotlin.math.roundToInt

fun newList(myList: List<Double>): List<Double>{
    val newList: MutableList<Double> = mutableListOf()
    for (item in myList){
        if (item.toInt() % 2 == 0)       {
           newList.add(item*item)
        }
        else {newList.add(item/2)}
    }
    return newList
}

fun Double.round(): Double {
    return (this * 100.0).roundToInt() / 100.0
}

fun filterList(myList: List<Double?>): Double {
    return newList(myList.filterNotNull())
        .asSequence()
        .filter { it < 25 }
        .sortedDescending()
        .take(10)
        .sum()
        .round()
}

fun main() {
    val firstList: List<Double?> = listOf(13.31, 3.98, 12.0, 2.99, 9.0, null)
    val secondList: List<Double?> = listOf(133.21, null, 233.98, null, 26.99, 5.0, 7.0, 9.0)

    println(filterList(firstList))
    println(filterList(secondList))
}
