package kotlin_advanced_1_13

import java.time.LocalDate
import java.util.logging.Handler
import kotlin.math.roundToInt

fun Int.square(): Int {
    return this * this
}

fun Double.round(): Any {
    return if (this.roundToInt() - this == 0.0) {
        this.roundToInt()
    }
    else (this * 100.0).roundToInt() / 100.0
}

fun compareDateAndTinkoffBDay(date: LocalDate): String {
    val tinkoffBDay = LocalDate.of(2006, 12, 24)
    return if (date < tinkoffBDay) {
        "меньше"
    } else {
        "не меньше"
    }
}

fun typeCasting(entity: Any?) {
    when (entity) {
        is String -> {
            println("Я получил тип String = \"$entity\", ее длина равна ${entity.length} символ")
        }

        is Int -> {
            println("Я получил Int = $entity, его квадрат равен ${entity.square()}")
        }

        is Double -> {
            println("Я получил Double = $entity, это число округляется до ${entity.round()}")
        }

        is LocalDate -> {
            println("Я получил LocalData = $entity, эта дата ${compareDateAndTinkoffBDay(entity)}, чем дата основания Tinkoff")
        }

        null -> {
            println("Я получил null объект")
        }

        else -> {
            println("Мне этот тип неизвестен(")
        }
    }
}

fun main() {
    typeCasting("Privet")
    typeCasting(145)
    typeCasting(145.0)
    typeCasting(145.2817812)
    typeCasting(LocalDate.of(1990, 1, 1))
    typeCasting(Handler::class)
    typeCasting(null)
}
