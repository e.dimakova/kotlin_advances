package kotlin_advanced_2_4

interface Swimmable {
    fun swim()
}
