package kotlin_advanced_1_9

fun createUser(
    name: String,
    surname: String,
    patronymic: String? = null,
    gender: String,
    age: Int,
    dateOfBirth: String,
    inn: String? = null,
    snils: String? = null
) {
    println(Person(name, surname, patronymic, gender, age, dateOfBirth, inn, snils).toString())
}

fun main() {
    createUser(
        age = 63,
        name = "Igor",
        gender = "FiveReasonMan",
        surname = "Nikolaev",
        dateOfBirth = "1960-01-30"
    )
}

